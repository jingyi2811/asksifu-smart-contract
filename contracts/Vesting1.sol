// SPDX-License-Identifier: MIT
pragma solidity 0.8.11;

import "hardhat/console.sol";

interface IErc20ContractObj {
    function transfer(address _recipient, uint _amount) external returns (bool);
    function balanceOf(address _tokenOwner) external view returns (uint256);
}

contract Vesting1 {
    address public _admin;

    IErc20ContractObj public _erc20ContractObj;
    address public _erc20ContractAddress;

    uint public _startDate;
    uint public _endDate;
    uint public _totalDistributionAmount;
    uint public _totalDistributionDays;
    uint public _distributionTimesPerDay;

    mapping(address => uint) public _recipientAddressTotalAmount;
    mapping(address => uint) public _recipientAddressLeftOverAmount;
    mapping(address => uint) public _recipientAddressLastPayBlockTimestamp;

    bool public _hasAddressNotYetConfirmed = true;

    constructor(
        uint startDate_,
        uint endDate_,
        uint distributionTimesPerDay_,
        address erc20ContractAddress_
    ) {
        require(endDate_ >= startDate_, 'End date must be greater or equal to start date');
        require(erc20ContractAddress_ != address(0), "Address cannot be zero");
        _admin = msg.sender;
        _startDate = startDate_;
        _endDate = endDate_;
        _totalDistributionDays = (endDate_ - startDate_) / 86400 + 1;
        _distributionTimesPerDay = distributionTimesPerDay_;
        _erc20ContractAddress = erc20ContractAddress_;
    }

    modifier onlyAdmin() {
        require(_admin == msg.sender);
        _;
    }

    // Set the recipient addresses and amounts
    function setRecipientAddressesAmounts(address[] calldata recipientAddressArr_, uint[] calldata amountArr_) external onlyAdmin {
        require(_hasAddressNotYetConfirmed,
            'Recipient addresses have already been confirmed');
        require(recipientAddressArr_.length == amountArr_.length, 'Size of recipient address array and amount array must be the same');
        for (uint i = 0; i < recipientAddressArr_.length; i++) {
            require(recipientAddressArr_[i] != address(0), "Address cannot be zero");
            require(amountArr_[i] > 0, "Amount cannot be 0");
            require(!_isContract(recipientAddressArr_[i]), 'Contracts are not allowed');
            _recipientAddressTotalAmount[recipientAddressArr_[i]] = amountArr_[i];
            _recipientAddressLeftOverAmount[recipientAddressArr_[i]] = amountArr_[i];
            _totalDistributionAmount = _totalDistributionAmount + amountArr_[i];
        }
    }

    // Confirm address and balance
    function confirmAddressAndBalance() external onlyAdmin {
        require(_hasAddressNotYetConfirmed, 'Unable to confirm address again');
        require(_erc20ContractObj.balanceOf(address(this)) >= _totalDistributionAmount);
        _hasAddressNotYetConfirmed = false;
    }

    // Distribute to the address based on
    // - Total distribution amount
    // - Distribution times per day
    function distribute() external {
        require(!_hasAddressNotYetConfirmed, "Address has not yet been confirmed");
        require(block.timestamp >= _startDate, "Current block timestamp is less than start date");
        uint amountLeft = _recipientAddressLeftOverAmount[msg.sender];
        require (amountLeft > 0, "All amount was paid");
        uint singleDistributionAmount = _recipientAddressTotalAmount[msg.sender] / _totalDistributionDays / _distributionTimesPerDay;
        uint maxNoOfTimesToPay = amountLeft / singleDistributionAmount;
        uint noOfTimesToPay = (block.timestamp - _startDate) * _distributionTimesPerDay / 86400 + 1;
        uint unpaidTimes = noOfTimesToPay - _recipientAddressLastPayBlockTimestamp[msg.sender];
        if (unpaidTimes > maxNoOfTimesToPay) unpaidTimes = maxNoOfTimesToPay;
        uint amount = unpaidTimes * singleDistributionAmount;
        if (amount > 0) {
            bool canTransfer = _erc20ContractObj.transfer(msg.sender, amount);
            if(canTransfer) {
                _recipientAddressLeftOverAmount[msg.sender] = _recipientAddressLeftOverAmount[msg.sender] - amount;
                _recipientAddressLastPayBlockTimestamp[msg.sender] = noOfTimesToPay;
            }
        }
    }

    // Allow admin to return any token accidentally sent to this contract
    // If the token address is _erc20ContractAddress, admin only allowed to return the token back after the end date
    function recoverToken(address tokenAddress_, address recoveryAddress_, uint amount_) public onlyAdmin {
        if(_erc20ContractAddress != recoveryAddress_) {
            IErc20ContractObj(tokenAddress_).transfer(recoveryAddress_, amount_);
        } else {
            if(block.timestamp > _endDate){
                IErc20ContractObj(tokenAddress_).transfer(recoveryAddress_, amount_);
            }
        }
    }

    // Reject all native tokens deposited to this contract
    receive() external payable {
        revert();
    }

    // Check if an address is a contract
    function _isContract(address addr_) internal view returns (bool) {
        uint size;
        assembly {
            size := extcodesize(addr_)
        }
        return size > 0;
    }
}