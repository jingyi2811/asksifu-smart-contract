// SPDX-License-Identifier: MIT
pragma solidity 0.8.11;

interface IVesting2Obj {
    function initialize(
        uint startDate_,
        uint endDate_,
        address erc20ContractAddress_,
        uint amount_
    ) external;
}

contract Create2 {

    mapping(address => address[]) public addressDetail;

    function create2(
        uint startDate_,
        uint endDate_,
        address erc20ContractAddress_,
        uint amount_
    ) external returns (address pair) {
        require(endDate_ >= startDate_, 'Date to must be greater or equal to date from');
        require(erc20ContractAddress_ != address(0), "Address cannot be zero");
        bytes memory bytecode = type(Vesting2).creationCode;
        bytes32 salt = keccak256(abi.encodePacked(startDate_, endDate_, erc20ContractAddress_, amount_));
        assembly {
            pair := create2(0, add(bytecode, 32), mload(bytecode), salt)
        }
        IVesting2Obj(pair).initialize(startDate_, endDate_, erc20ContractAddress_, amount_);
        addressDetail[msg.sender].push(pair);
    }
}

interface IErc20ContractObj {
    function transfer(address recipient, uint amount) external returns (bool);
    function balanceOf(address tokenOwner) external view returns (uint256);
}

contract Vesting2 {
    address public _admin = 0xc6c62286137969846F1409A4c0d169eDcA4100a8;
    address public _recipient;

    IErc20ContractObj public _erc20ContractObj;
    address public _erc20ContractAddress;

    uint public _startDate;
    uint public _endDate;
    uint public _dayToPay;

    uint public _recipientAddressTotalAmount;
    uint public _recipientAddressLeftAmount;
    uint public _paidDays;

    bool public _hasConfirmed;

    constructor() {
        _recipient = msg.sender;
    }

    function initialize(
        uint startDate_,
        uint endDate_,
        address erc20ContractAddress_,
        uint amount_
    ) external onlyRecipient {
        require(!_hasConfirmed, 'Already confirmed before');
        require(endDate_ >= startDate_, 'Date to must be greater or equal to date from');
        require(erc20ContractAddress_ != address(0), "Address cannot be zero");
        _startDate = startDate_;
        _endDate = endDate_;
        _dayToPay = (endDate_ - startDate_) / 86400 + 1;
        _erc20ContractObj = IErc20ContractObj(erc20ContractAddress_);
        _erc20ContractAddress = erc20ContractAddress_;
        _recipientAddressTotalAmount = amount_;
        _recipientAddressLeftAmount = amount_;
    }

    modifier onlyAdmin() {
        require(_admin == msg.sender);
        _;
    }

    modifier onlyRecipient() {
        require(_recipient == msg.sender);
        _;
    }

    function confirm() external onlyRecipient {
        require(!_hasConfirmed, 'Already confirmed before');
        _hasConfirmed = true;
    }

    function distribute() external {
        require(_hasConfirmed, "Not yet confirmed");
        require(block.timestamp >= _startDate, 'current block timestamp is less than date from');
        uint amountLeft = _recipientAddressLeftAmount;
        require (amountLeft > 0, "All amount was paid");
        uint dailyAmount = _recipientAddressTotalAmount / _dayToPay;
        uint maxDayToPay = amountLeft / dailyAmount;
        uint day = (block.timestamp - _startDate) / 86400 + 1;
        uint unpaidDays = day - _paidDays;
        if (unpaidDays > maxDayToPay) unpaidDays = maxDayToPay;
        uint amount = unpaidDays * dailyAmount;
        if (amount > 0) {
            _recipientAddressLeftAmount = _recipientAddressLeftAmount - amount;
            bool canTransfer = _erc20ContractObj.transfer(_recipient, amount);
            if(canTransfer) {
                _paidDays = day;
            }
        }
    }

    receive() external payable {
        revert();
    }

    function recoverToken(uint amount_) public onlyRecipient {
        require(!_hasConfirmed || block.timestamp > _endDate, "has confirmed or timestamp <= endDate");
        IErc20ContractObj(_erc20ContractAddress).transfer(_recipient, amount_);
    }

    function recoverOtherToken(address tokenAddress_, address recoveryAddress_, uint amount_) public onlyAdmin {
        require(_erc20ContractAddress != recoveryAddress_, "Token must not be the same");
        IErc20ContractObj(tokenAddress_).transfer(recoveryAddress_, amount_);
    }

    function _isContract(address addr_) internal view returns (bool) {
        uint size;
        assembly {
            size := extcodesize(addr_)
        }
        return size > 0;
    }
}