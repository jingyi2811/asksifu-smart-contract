import { ethers } from "hardhat";
import { getUnixTime } from 'date-fns';
import {expect} from "chai";

describe("Deployment", function () {
  let deployer: any;

  // Initial setup
  const startDate = getUnixTime(new Date('2022/01/01'));
  const endDate = getUnixTime(new Date('2022/12/31'));

  let ERC20Mock: any;
  let vesting: any;

  before(async function () {
    [deployer] = await ethers.getSigners();

    const vestingContractFactory = await ethers.getContractFactory("Vesting1");
    const erc20MockContractFactory = await ethers.getContractFactory("ERC20Mock");

    ERC20Mock = await erc20MockContractFactory.deploy(219000);
    await ERC20Mock.deployed();

    vesting = await vestingContractFactory.deploy(
        startDate,
        endDate,
        2,
        ERC20Mock.address
    )
    await vesting.deployed();
  });

  describe("Deployment", function () {
    it("Should set the right owner", async function () {
      expect(await vesting._admin()).to.equal(deployer.address);
    });

    it("Should set the right start date", async function () {
      expect(await vesting._startDate()).to.equal(startDate);
    });

    it("Should set the right end date", async function () {
      expect(await vesting._endDate()).to.equal(endDate);
    });

    it("Distribution times per day", async function () {
      expect(await vesting._distributionTimesPerDay()).to.equal(2);
    });

    it("Should have balance", async function () {
      expect(await ERC20Mock.balanceOf(deployer.address)).to.equal(
          '219000000000000000000000'
      );
    });
  });
});