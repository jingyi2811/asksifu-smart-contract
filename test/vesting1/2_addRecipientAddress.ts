import { ethers } from "hardhat";
import { getUnixTime } from 'date-fns';
import {expect} from "chai";
import {BigNumber} from "ethers";

describe("Add recipient address", function () {
  let deployer: any;

  // Initial setup
  const startDate = getUnixTime(new Date('2022/01/01'));
  const endDate = getUnixTime(new Date('2022/12/31'));

  let ERC20Mock: any;
  let vesting: any;

  let account1:any;
  let account2:any;
  let account3:any;

  beforeEach(async function () {
    [deployer, account1, account2, account3] = await ethers.getSigners();

    const vestingContractFactory = await ethers.getContractFactory("Vesting1");
    const erc20MockContractFactory = await ethers.getContractFactory("ERC20Mock");

    ERC20Mock = await erc20MockContractFactory.deploy(219000);
    await ERC20Mock.deployed();

    vesting = await vestingContractFactory.deploy(
        startDate,
        endDate,
        2,
        ERC20Mock.address
    )
    await vesting.deployed();
  });

  describe("Set recipient addresses", function () {
    it("Should set recipient addresses 1", async function () {
      expect(await vesting._hasAddressNotYetConfirmed()).to.equal(true);

      const eighteen_decimal_fractional_part = "000000000000000000";

      await vesting.setRecipientAddressesAmounts(
          [
              account1.address,
              account2.address,
              account3.address
          ],
          [
              "36500" + eighteen_decimal_fractional_part,
              "73000" + eighteen_decimal_fractional_part,
              "109500" + eighteen_decimal_fractional_part
          ]
      );

      {
        let account1Balance: BigNumber = await vesting._recipientAddressTotalAmount(account1.address)
        expect(ethers.utils.formatEther(account1Balance)).to.equal("36500.0");

        let account2Balance: BigNumber = await vesting._recipientAddressTotalAmount(account2.address)
        expect(ethers.utils.formatEther(account2Balance)).to.equal("73000.0");

        let account3Balance: BigNumber = await vesting._recipientAddressTotalAmount(account3.address)
        expect(ethers.utils.formatEther(account3Balance)).to.equal("109500.0");
      }

      {
        let account1Balance: BigNumber = await vesting._recipientAddressLeftOverAmount(account1.address)
        expect(ethers.utils.formatEther(account1Balance)).to.equal("36500.0");

        let account2Balance: BigNumber = await vesting._recipientAddressLeftOverAmount(account2.address)
        expect(ethers.utils.formatEther(account2Balance)).to.equal("73000.0");

        let account3Balance: BigNumber = await vesting._recipientAddressLeftOverAmount(account3.address)
        expect(ethers.utils.formatEther(account3Balance)).to.equal("109500.0");
      }

      await ERC20Mock.connect(deployer).transfer(vesting.address, "219000" + eighteen_decimal_fractional_part)

      await vesting.confirmAddressAndBalance();
    });

    it("Should set recipient addresses 2", async function () {
      expect(await vesting._hasAddressNotYetConfirmed()).to.equal(true);

      const eighteen_decimal_fractional_part = "000000000000000000";

      await vesting.setRecipientAddressesAmounts(
          [
            account1.address,
            account2.address,
            account3.address
          ],
          [
            "18250" + eighteen_decimal_fractional_part,
            "21900" + eighteen_decimal_fractional_part,
            "25550" + eighteen_decimal_fractional_part
          ]
      );

      {
        let account1Balance: BigNumber = await vesting._recipientAddressTotalAmount(account1.address)
        expect(ethers.utils.formatEther(account1Balance)).to.equal("18250.0");

        let account2Balance: BigNumber = await vesting._recipientAddressTotalAmount(account2.address)
        expect(ethers.utils.formatEther(account2Balance)).to.equal("21900.0");

        let account3Balance: BigNumber = await vesting._recipientAddressTotalAmount(account3.address)
        expect(ethers.utils.formatEther(account3Balance)).to.equal("25550.0");
      }

      {
        let account1Balance: BigNumber = await vesting._recipientAddressLeftOverAmount(account1.address)
        expect(ethers.utils.formatEther(account1Balance)).to.equal("18250.0");

        let account2Balance: BigNumber = await vesting._recipientAddressLeftOverAmount(account2.address)
        expect(ethers.utils.formatEther(account2Balance)).to.equal("21900.0");

        let account3Balance: BigNumber = await vesting._recipientAddressLeftOverAmount(account3.address)
        expect(ethers.utils.formatEther(account3Balance)).to.equal("25550.0");
      }

      await ERC20Mock.connect(deployer).transfer(vesting.address, "65700" + eighteen_decimal_fractional_part)

      await vesting.confirmAddressAndBalance();
    });
  });
});
