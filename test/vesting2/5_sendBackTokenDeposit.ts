import { ethers } from "hardhat";
import { getUnixTime } from 'date-fns';
import {expect} from "chai";

describe("Vesting", function () {
  let deployer: any;
  let account1: any;

  let VestingContractFactory: any;

  // Initial setup
  const startDate = getUnixTime(new Date('2023/01/01'));
  const endDate = getUnixTime(new Date('2023/12/31'));

  let ERC20Mock: any;
  let Vesting: any;

  before(async function () {
    [deployer, account1] = await ethers.getSigners();

    VestingContractFactory = await ethers.getContractFactory("Vesting2");
    const erc20MockContractFactory = await ethers.getContractFactory("ERC20Mock");

    ERC20Mock = await erc20MockContractFactory.deploy(219000);
    await ERC20Mock.deployed();

    Vesting = await VestingContractFactory.deploy()
    await Vesting.deployed();

    await Vesting.connect(deployer).initialize(
        startDate,
        endDate,
        ERC20Mock.address,
        11111
    )
  });

  describe("Send back token deposit", function () {
    it("Send back token deposit", async function () {

    });
  });
});
