import { ethers } from "hardhat";
import {getUnixTime} from "date-fns";

describe("Create2", function () {
  let deployer: any;
  let create2: any;
  let ERC20Mock: any;

  before(async function () {
    [deployer] = await ethers.getSigners();
    const create2ContractFactory = await ethers.getContractFactory("Create2");
    create2 = await create2ContractFactory.deploy();
    await create2.deployed();
  });

  describe("Create2", async function () {
    // Initial setup
    const startDate = getUnixTime(new Date('2023/01/01'));
    const endDate = getUnixTime(new Date('2023/12/31'));
    const erc20MockContractFactory = await ethers.getContractFactory("ERC20Mock");
    ERC20Mock = await erc20MockContractFactory.deploy(219000);

    await create2.create2(
        startDate,
        endDate,
        1,
        ERC20Mock.address
    )
  });
});