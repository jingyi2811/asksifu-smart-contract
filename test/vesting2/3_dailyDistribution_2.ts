import { ethers } from "hardhat";
import { getUnixTime } from 'date-fns';
import {expect} from "chai";

describe("Vesting", function () {
  let deployer: any;
  let VestingContractFactory: any;

  // Initial setup
  const startDate = getUnixTime(new Date('2023/01/01'));
  const endDate = getUnixTime(new Date('2024/12/30'));

  let ERC20Mock: any;
  let Vesting: any;
  const eighteen_decimal_fractional_part = "000000000000000000";

  before(async function () {
    [deployer] = await ethers.getSigners();

    VestingContractFactory = await ethers.getContractFactory("Vesting2");
    const erc20MockContractFactory = await ethers.getContractFactory("ERC20Mock");

    ERC20Mock = await erc20MockContractFactory.deploy("219000");
    await ERC20Mock.deployed();

    Vesting = await VestingContractFactory.deploy()
    await Vesting.deployed();

    await Vesting.connect(deployer).initialize(
        startDate,
        endDate,
        ERC20Mock.address,
        219000 + eighteen_decimal_fractional_part
    )
  });

  describe("Daily distribution", function () {
    it("Should distribute", async function () {
      const eighteen_decimal_fractional_part = "000000000000000000";
      await ERC20Mock.connect(deployer).transfer(Vesting.address, "219000" + eighteen_decimal_fractional_part)
      await Vesting.confirm();

      {
        // Before any transfer made
        const balance = await ERC20Mock.balanceOf(deployer.address)
        expect(ethers.utils.formatEther(balance)).to.equal("0.0");

        const dailyAmount = await Vesting._recipientAddressTotalAmount()
        expect(ethers.utils.formatEther(dailyAmount)).to.equal("219000.0");

        const leftAmount = await Vesting._recipientAddressLeftAmount()
        expect(ethers.utils.formatEther(leftAmount)).to.equal("219000.0");

        const dayToPay = await Vesting._dayToPay()
        expect(dayToPay).to.equal("730");
      }

      {
        // 2nd day
        const time = startDate + 86400 * (2 - 1) // Set to 2 days later
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await Vesting.connect(deployer).distribute()

        const balance = await ERC20Mock.balanceOf(deployer.address)
        expect(ethers.utils.formatEther(balance)).to.equal("600.0");
      }

      {
        // 5th day
        const time = startDate + 86400 * (5 - 1) // Set to 5 days later
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await Vesting.connect(deployer).distribute()

        const balance = await ERC20Mock.balanceOf(deployer.address)
        expect(ethers.utils.formatEther(balance)).to.equal("1500.0");
      }

      {
        // 200th day
        const time = startDate + 86400 * (200 - 1) // Set to 200 days later
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await Vesting.connect(deployer).distribute()

        const balance = await ERC20Mock.balanceOf(deployer.address)
        expect(ethers.utils.formatEther(balance)).to.equal("60000.0");
      }

      {
        // 364th day
        const time = startDate + 86400 * (364 - 1) // Set to 364 days later
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await Vesting.connect(deployer).distribute()

        const balance = await ERC20Mock.balanceOf(deployer.address)
        expect(ethers.utils.formatEther(balance)).to.equal("109200.0");
      }

      {
        // 366th day
        const time = startDate + 86400 * (366 - 1) // Set to 366 days later
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await Vesting.connect(deployer).distribute()

        const balance = await ERC20Mock.balanceOf(deployer.address)
        expect(ethers.utils.formatEther(balance)).to.equal("109800.0");
      }

      {
        // 500th day
        const time = startDate + 86400 * (500 - 1) // Set to 500 days later
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await Vesting.connect(deployer).distribute()

        const balance = await ERC20Mock.balanceOf(deployer.address)
        expect(ethers.utils.formatEther(balance)).to.equal("150000.0");
      }

      {
        // 729th day
        const time = startDate + 86400 * (729 - 1) // Set to 729 days later
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await Vesting.connect(deployer).distribute()

        const balance = await ERC20Mock.balanceOf(deployer.address)
        expect(ethers.utils.formatEther(balance)).to.equal("218700.0");
      }

      {
        // 731th day
        const time = startDate + 86400 * (731 - 1) // Set to 731 days later
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await Vesting.connect(deployer).distribute()

        const balance = await ERC20Mock.balanceOf(deployer.address)
        expect(ethers.utils.formatEther(balance)).to.equal("219000.0");
      }
    });
  });
});