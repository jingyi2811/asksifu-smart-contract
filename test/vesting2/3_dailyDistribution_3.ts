import { ethers } from "hardhat";
import { getUnixTime } from 'date-fns';
import {expect} from "chai";

describe("Vesting", function () {
  let deployer: any;
  let VestingContractFactory: any;

  // Initial setup
  const startDate = getUnixTime(new Date('2023/01/01'));
  const endDate = getUnixTime(new Date('2023/01/30'));

  let ERC20Mock: any;
  let Vesting: any;
  const eighteen_decimal_fractional_part = "000000000000000000";

  before(async function () {
    [deployer] = await ethers.getSigners();

    VestingContractFactory = await ethers.getContractFactory("Vesting2");
    const erc20MockContractFactory = await ethers.getContractFactory("ERC20Mock");

    ERC20Mock = await erc20MockContractFactory.deploy("11111");
    await ERC20Mock.deployed();

    Vesting = await VestingContractFactory.deploy()
    await Vesting.deployed();

    await Vesting.connect(deployer).initialize(
        startDate,
        endDate,
        ERC20Mock.address,
        11111
    )
  });

  describe("Daily distribution", function () {
    const eighteen_decimal_fractional_part = "000000000000000000";

    it("Should distribute", async function () {

      await ERC20Mock.connect(deployer).transfer(Vesting.address, "11111" + eighteen_decimal_fractional_part)
      await Vesting.confirm();

      {
        // Before any transfer made
        const balance = await ERC20Mock.balanceOf(deployer.address)
        expect(ethers.utils.formatEther(balance)).to.equal("0.0");

        {
          const dailyAmount = await Vesting._recipientAddressTotalAmount()
          expect(dailyAmount).to.equal("11111");

          const leftAmount = await Vesting._recipientAddressLeftAmount()
          expect(dailyAmount).to.equal("11111");
        }

        const dayToPay = await Vesting._dayToPay()
        expect(dayToPay).to.equal("30");
      }

      {
        // 2nd day
        const day = 2;

        const time = startDate + 86400 * (day - 1)
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await Vesting.connect(deployer).distribute()

        {
          const balance = await ERC20Mock.balanceOf(deployer.address)
          expect(ethers.utils.formatUnits(balance, 0)).to.equal((370  * day).toString());
        }
      }

      {
        // 20th day
        const day = 20;

        const time = startDate + 86400 * (day - 1)
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await Vesting.connect(deployer).distribute()

        {
          const balance = await ERC20Mock.balanceOf(deployer.address)
          expect(ethers.utils.formatUnits(balance, 0)).to.equal((370 * day).toString());
        }
      }

      {
        // 30th day
        const day = 30;

        const time = startDate + 86400 * (day - 1)
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await Vesting.connect(deployer).distribute()

        {
          const balance = await ERC20Mock.balanceOf(deployer.address)
          expect(ethers.utils.formatUnits(balance, 0)).to.equal((370 * day).toString());
        }
      }

      {
        // 31th day
        const day = 31;

        const time = startDate + 86400 * (day - 1)
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await Vesting.connect(deployer).distribute()

        {
          const balance = await ERC20Mock.balanceOf(deployer.address)
          expect(ethers.utils.formatUnits(balance, 0)).to.equal((370 * 30).toString());
        }
      }
    });
  });
});