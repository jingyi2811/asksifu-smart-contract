import { ethers } from "hardhat";
import { getUnixTime } from 'date-fns';
import {expect} from "chai";

describe("Vesting", function () {
  let deployer: any;

  // Initial setup
  const startDate = getUnixTime(new Date('2023/01/01'));
  const endDate = getUnixTime(new Date('2023/12/31'));

  let ERC20Mock: any;
  let Vesting: any;

  before(async function () {
    [deployer] = await ethers.getSigners();

    const VestingContractFactory = await ethers.getContractFactory("Vesting2");
    const erc20MockContractFactory = await ethers.getContractFactory("ERC20Mock");

    ERC20Mock = await erc20MockContractFactory.deploy("219000");
    await ERC20Mock.deployed();

    Vesting = await VestingContractFactory.deploy()
    await Vesting.deployed();

    await Vesting.connect(deployer).initialize(
        startDate,
        endDate,
        ERC20Mock.address,
        219000
    )
  });

  describe("Deployment", function () {
    it("Should set the correct admin", async function () {
      expect(await Vesting._admin()).to.equal('0xc6c62286137969846F1409A4c0d169eDcA4100a8');
    });

    it("Should set the correct recipient", async function () {
      expect(await Vesting._recipient()).to.equal(deployer.address);
    });

    it("Should set the correct erc20 contract address", async function () {
      expect(await Vesting._erc20ContractAddress()).to.equal(ERC20Mock.address);
    });

    it("Should set the correct date from", async function () {
      expect(await Vesting._startDate()).to.equal(startDate);
    });

    it("Should set the correct date to", async function () {
      expect(await Vesting._endDate()).to.equal(endDate);
    });

    it("Should set the correct day to pay", async function () {
      expect(await Vesting._dayToPay()).to.equal(
          365
      );
    });

    it("Should set the correct recipient address total amount", async function () {
      expect(await Vesting._recipientAddressTotalAmount()).to.equal(
          219000
      );
    });

    it("Should set the correct recipient address left amount", async function () {
      expect(await Vesting._recipientAddressLeftAmount()).to.equal(
          219000
      );
    });

    it("Should have balance", async function () {
      expect(await ERC20Mock.balanceOf(deployer.address)).to.equal(
          '219000000000000000000000'
      );
    });
  });
});